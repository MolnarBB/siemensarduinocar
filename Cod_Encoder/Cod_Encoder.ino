
#define encoderPinA 5
#define encoderPinB 3
#define PPR 1023.0     //pulse per revolution
#define fullCircle 360

volatile int counter = 0;
volatile int angle = 0;
volatile boolean wheelStateChanged;

void setup() {

  Serial.begin (9600);

  pinMode(encoderPinA, INPUT);
  pinMode(encoderPinB, INPUT);

  attachInterrupt(digitalPinToInterrupt(encoderPinB), countSignal, RISING);


}

void loop() {
  if (wheelStateChanged == true) {

    angle = (fullCircle / PPR) * counter;  //0.35xx * counter [-1023..1023] = [-360 .. 360]

    if (Circle())
    {
      resetEncoder();
    }

    Serial.println(angle);

    wheelStateChanged = false;
  }
}

//Interrupts

bool Circle()
{
  return (angle >= fullCircle) || (angle <= (-fullCircle));  //encoder rotated 360 degrees
}

void countSignal() {

  wheelStateChanged = true;

  if (digitalRead(encoderPinA) == HIGH) {
      if (digitalRead(encoderPinB) == LOW) {
        counter--; //COUNTER CLOCK WISE
      }
      else {
        counter++; //CW
      }
    }
    else {
      if (digitalRead(encoderPinB) == LOW) {
        counter++; //CW
      }
      else {
        counter--; //CCW
      }
    }
}


void resetEncoder()
{
  counter = 0;
  angle = 0;
}

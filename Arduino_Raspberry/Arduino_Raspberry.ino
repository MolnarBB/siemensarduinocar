#include <SPI.h>
#include <Servo.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include <string.h>

// Global Variables
unsigned char sentBytes[4];
unsigned char receivedBytes[4];

//LiquidCrystal_I2C lcd(0x27, 20, 4); // set the LCD address to 0x27 for a 16 chars and 2 line display
Servo myservo;

#define encoderPin0 2
#define encoderPinA 3
#define encoderPinB 5
#define PPR 1024.0     //pulse per revolution
#define fullCircle 360

static const int servoPin = 9;
int posServo = 0;

volatile  int counter = 0;
volatile  float angle = 0.;                //variables for Encoder


union FloatUL {
  float fValue;
  uint32_t ulValue;
};

void serializeFloat(unsigned char *outByteArray, float floatToSerialize)
{
  FloatUL floatUl;
  floatUl.fValue = floatToSerialize;
  for (int iByte = 0; iByte < 4; iByte++)
  {
    outByteArray[iByte] = floatUl.ulValue >> ((3 - iByte) * 8);
  }
}

float deserializeFloat(unsigned char *inByteArray)
{
  FloatUL floatUl;
  floatUl.ulValue = 0.0f;
  for (int iByte = 0; iByte < 4; iByte++)
  {
    floatUl.ulValue |= (uint32_t)inByteArray[iByte] << ((3 - iByte) * 8);
  }
  return floatUl.fValue;
}

void requestEvent()
{
    serializeFloat(sentBytes, (angle < -40) ? -40 : ((angle > 40) ? 40 : angle));
    
    char sendableBytes[4];
    memcpy(sendableBytes, sentBytes, 4);

    //Serial.println((angle < -40) ? -40 : ((angle > 40) ? 40 : angle));
    Wire.write(sendableBytes, 4);
}

void receiveEvent(int numBytes)
{
    char receivableBytes[4];
    for(int iByte = 0; iByte < 4; ++iByte)
    {
        receivableBytes[iByte] = Wire.read();
    }
    memcpy(receivedBytes, receivableBytes, 4);
   float receivedAngle = deserializeFloat(receivedBytes);
   Serial.println(receivedAngle);
   posServo = map(receivedAngle, -40, 40, 83, 120); 
   Serial.println("Servo:");
   Serial.println(posServo);
   myservo.write(posServo);    
}


int setPosition(float angle)
{
  if (angle >= 130)
    return 130;
  if (angle <= 73)
    return 73;

  return angle;
}

void countSignal() {

  if (digitalRead(encoderPinB) == HIGH) {
        counter--; //COUNTER CLOCK WISE
      }
      else {
        counter++; //CW
      }

   angle = (fullCircle / PPR) * counter;  //0.35xx * counter [-1023..1023] = [-360 .. 360]
}


void resetEncoder()
{
  counter = 0;
  angle = 0;
}//

void setup() {
  
  Serial.begin(2000000);
  Wire.begin(0x07); //Set Arduino up as an I2C slave at address 0x07
  Wire.onRequest(requestEvent); //Prepare to send data
  Wire.onReceive(receiveEvent); //Prepare to recieve data
  
  myservo.attach(servoPin);  // attaches the servo on the servoPin to the servo object
  myservo.write(90);  //set servo to default value

  pinMode(encoderPin0, INPUT);
  pinMode(encoderPinA, INPUT);
  pinMode(encoderPinB, INPUT);

  attachInterrupt(digitalPinToInterrupt(encoderPinA), countSignal, RISING);
  attachInterrupt(digitalPinToInterrupt(encoderPin0), resetEncoder, RISING);
}

void loop() {  }

#include "WiFiEsp.h"
#include <SPI.h>
#include <Servo.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 20, 4); // set the LCD address to 0x27 for a 16 chars and 2 line display

#ifndef HAVE_HWSERIAL1
HardwareSerial & espSerial = Serial1; //pin 19(RX) & pin 18(TX)
#endif

Servo myservo;  // create servo object to control a servo

//servo is attached to
static const int servoPin = 9;

// Replace with your network credentials
char ssid[] = "RRV";            // your network SSID (name)
char pass[] = "r3al1tat3v1rtuala";        // your network password
int status = WL_IDLE_STATUS;     // the Wifi radio's status
int reqCount = 0;                // number of requests received

// Set web server port number to 1335
WiFiEspServer server(1335);
IPAddress ip(10, 90, 142, 222);

WiFiEspClient clientTCP;
IPAddress TcpServer(10, 90, 142, 221);

// Variable to store the HTTP request
String header;

String valueString = String(90);
int pos1 = 0;
int pos2 = 0;
int posServo = 0;

void setup() {

  lcd.init();                
  lcd.backlight();
  lcd.setCursor(0, 0);
  
  myservo.attach(servoPin);  // attaches the servo on the servoPin to the servo object

  // initialize serial for debugging
  Serial.begin(115200);
  // initialize serial for ESP module
  Serial1.begin(115200);
  // initialize ESP module
  WiFi.init(&Serial1);

  // check for the presence of the shield
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    lcd.print("Esp not present!");
    // don't continue
    while (true);
  }

//  WiFi.config(ip);      //set ESP IP

  lcd.setCursor(0, 0);
  lcd.print("IP:10.90.142.222");
  lcd.setCursor(0, 1);
  lcd.print("Port: 1335");
  delay(2500);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Connecting to ");
  lcd.setCursor(0, 1);
  lcd.print("WiFi: ");
  lcd.print(ssid);
  delay(1000);
  
  // attempt to connect to WiFi network
  while ( status != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network
    status = WiFi.begin(ssid, pass);               
  }

  Serial.println("You're connected to the network");
  lcd.clear();
  lcd.print("WiFi connected!");
  printWifiStatus();

  // start the web server on port 1335
  server.begin();

  Serial.println("\nStarting connection...");

  lcd.clear();
  lcd.print("Connecting to ");
  lcd.setCursor(0, 1);
  lcd.print("TCP, port: 1030");

  while (!clientTCP.connect(TcpServer, 1030)) {      //connect to TCP
    delay(10); 
  }

  if (clientTCP.connected()) {
    Serial.println("connected");

    lcd.clear();
    lcd.print("Connected to TCP");
    delay(1000);
    lcd.clear();
  }

}

void loop() {
  WiFiEspClient client = server.available();   // Listen for incoming clients


  /*int val = analogRead(0);    
  val = map(val, 0, 1023, -35, 35);
  val = setPosition(val);
  Serial.println(val);*/
              
  if (client) {                             // If a client connects,
    Serial.println("New Client");          
    String currentLine = "";                // make a String to hold incoming data from the client

    while (client.connected()) {            
      if (client.available()) {            
        char c = client.read();             // read a byte
        Serial.write(c);                    
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            lcd.setCursor(4, 0);
            lcd.print("Wait");
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();

            // Display the HTML web page
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            // CSS to style the on/off buttons
       
            client.println("<style>body { text-align: center; font-family: \"Trebuchet MS\", Arial; margin-left:auto; margin-right:auto;}");
            client.println(".slider { width: 300px; }</style>");
            client.println("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>");             //jquery host

            // Web Page
            client.println("</head><body><h1>Servo</h1>");
            client.println("<p>Position: <span id=\"servoPos\"></span></p>");
            client.println("<input type=\"range\" min=\"0\" max=\"180\" class=\"slider\" id=\"servoSlider\" onchange=\"servo(this.value)\" value=\"" + valueString + "\"/>");
            client.println("<br> </br>");
            client.println("<button id=\"plus\">plus</button>");
            client.println("<button id=\"minus\">minus</button>");

            client.println("<script>var slider = document.getElementById(\"servoSlider\");");
            client.println("var servoP = document.getElementById(\"servoPos\"); servoP.innerHTML = slider.value;");
            client.println("slider.oninput = function() { slider.value = this.value; servoP.innerHTML = this.value; }");
            client.println("$.ajaxSetup({timeout:10000}); function servo(pos) { ");
            client.println("$.get(\"/?value=\" + pos + \"&\");");
            client.println("$(document).ready(function() {$(\"#plus\").click(function(e){ var sum =  parseInt($(\"#servoSlider\").val(),10) + 5;$(\"#servoSlider\").val(sum);alert(sum);servoP = document.getElementById(\"servoPos\"); servoP.innerHTML = slider.value;servo(sum);e.stopImmediatePropagation(); }); e.preventDefault();});");
            client.println("$(document).ready(function() {$(\"#minus\").click(function(e){ var dif =  parseInt($(\"#servoSlider\").val(),10) - 5;$(\"#servoSlider\").val(dif);alert(dif);servoP = document.getElementById(\"servoPos\"); servoP.innerHTML = slider.value;servo(dif);e.stopImmediatePropagation(); }); e.preventDefault();});");
            client.println("{Connection: close};}</script>");
            client.println("</body></html>");

            //GET /?value=180& HTTP/1.1
            if (header.indexOf("GET /?value=") >= 0) {
              pos1 = header.indexOf('=');
              pos2 = header.indexOf('&');
              valueString = header.substring(pos1 + 1, pos2);

              char valueStringTCP[3];
              valueString.toCharArray(valueStringTCP, valueString.length() + 1);        //get TCP in characters 

              posServo = setPosition(valueString.toInt());          
              setPosition(posServo);               //set servo position between -35 and 35
             
              lcd.setCursor(0, 0);
              lcd.print("   ");
              lcd.setCursor(0, 0);

              while (!clientTCP.connected())
              {
                lcd.setCursor(0, 0);
                lcd.print("TCP DISCONNECTED");
                lcd.setCursor(0, 1);
                lcd.print("RECONNECTING");
                delay(1000);
                
                clientTCP.connect(TcpServer, 1030);
                if (clientTCP.connected())
                {
                  lcd.clear();
                  //lcd.print("");
                }
              }
              
              if (clientTCP.connected()) {
                clientTCP.write(valueStringTCP);         //send TCP IP
                lcd.print(posServo);
                lcd.setCursor(4, 0);
                lcd.print("Wait");
              }

              //Rotate the servo
              myservo.write(posServo);
              Serial.println(valueString);

            }
            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = " ";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    lcd.setCursor(4, 0);
    lcd.print("  OK");
    Serial.println("");
  }
}

void printWifiStatus()
{
  // print the SSID of the network you're attached to
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print where to go in the browser
  Serial.println();
  Serial.print("To see this page in action, open a browser to http://");
  Serial.println(ip);
  Serial.println();
}

int setPosition(int angle)
{
  if(angle > 130)
    return 130;
  if(angle < 73)
   return 73;

  return angle;
}
